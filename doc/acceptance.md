# Homer MergeRequest Acceptance process

The Acceptance process slightly differs whether you are a project's *developer* (or higher) or not.

### Voting system

#### General rules

- Only project *developers* (with a *developer* role in the project or higher) votes are taken into account.
- Votes on your own Merge Request (either positive or negative) are ignored.

#### New Method

- The **Score** is computed by counting the current number of approvals from the project *developers*.

Note: there is no real replacement for downvotes (:thumbsdown:) from the old method: this action is merged with the unresolved thread requirement.

#### Old method

- The **Score** is computed by comparing the number of :thumbsup: and :thumbsdown: given to the MergeRequest.

### Process Diagram

![](acceptance.svg)


# Implementation

Homer uses a simple decision graph to determine the status of the merge requests.

### Status Definitions

Visible status:
- NotCompliant: This MR doesn't pass the mandatory requirements such as a successful pipeline
- Reviewable: This MR has passed the pipeline and can now be reviewed by developers
- InReview: This MR is being reviewed and has opened discussions
- Acceptable: This MR will be accepted once the cooldown period has passed (24h or 72h)
- Accepted: This MR can be rebased and merged by a developer
- Stale: This MR is not acceptable and inactive for too long (60 days)
- WaitingForReviewerFeedback: This MR review process will be paused until the explicit Reviewer add some feedback to the MR (comment, thread, emoji, approval).

Internal status:
- Submitted: Initial status
- PipelineReady: Is it possible to check the pipeline result
- QuantumState: It is not possible to know if the merge request is mergeable (the mergeability check is not finished)
- PipelineNotFinished: The pipeline check is not finished.


### If the MergeRequest author has Developer role (or higher)

```mermaid
graph TD
    Submitted([Submitted]) --> Q1{Is it mergeable?}
    Q1 -->|Yes| PipelineReady([PipelineReady])
    Q1 -->|No| NotCompliant([NotCompliant])
    Q1 -->|Cannot determine| QuantumState([QuantumState])
    PipelineReady --> Q2{Is CI OK?}
    Q2 -->|Yes| Reviewable([Reviewable])
    Q2 -->|No| NotCompliant
    Q2 -->|Cannot determine| PipelineNotFinished([PipelineNotFinished])
    Reviewable -->|"An explicit Reviewer has been set and the Reviewer never participated to the MR"| WaitingForReviewerFeedback([WaitingForReviewerFeedback])
    Reviewable -->|"No vote (1) and No thread opened during 72h (2)"| Accepted([Accepted])
    Reviewable -->|"Thread opened or another Developer voted"| InReview([InReview])
    InReview -->|"All threads resolved and Score >= 0"| Acceptable([Acceptable])
    InReview -->|"Inactive for 60 days"| Stale([Stale])
    Acceptable -->|Wait for 24h| Accepted
classDef default fill:#fff,stroke-width:2px
classDef internal stroke-dasharray: 5 5
class Submitted,PipelineReady,QuantumState,PipelineNotFinished internal
style Acceptable color:#8fbc8f,stroke:#8fbc8f
style Accepted color:#009966,stroke:#009966
style InReview color:#FF8800,stroke:#FF8800
style NotCompliant color:#6699cc,stroke:#6699cc
style Reviewable color:#FF610A,stroke:#FF610A
style Stale color:#6699cc,stroke:#6699cc
```

#### Notes

*(1)*: Upvotes/approval only do no extend the 72h timer. A downvote or a thread discussion will inhibit this graph transition.

*(2)*: 72h after the creation time of the MR, if there is no new version of it. The exact deadline is the later of:
- `(time of the first version of the MR or out-of-draft time) + 72h`
- `(time of the last "non simple/fast-forward rebase" version of the MR) + 24h`


### If the MergeRequest author is **NOT** a project's developer

```mermaid
graph TD
    Submitted([Submitted]) --> Q1{Is it mergeable?}
    Q1 -->|Yes| PipelineReady([PipelineReady])
    Q1 -->|No| NotCompliant([NotCompliant])
    Q1 -->|Cannot determine| QuantumState([QuantumState])
    PipelineReady --> Q2{Is CI OK?}
    Q2 -->|Cannot determine| PipelineNotFinished([PipelineNotFinished])
    Q2 -->|No| NotCompliant
    Q2 -->|Yes| Reviewable([Reviewable])
    Reviewable -->|"An explicit Reviewer has been set and the Reviewer never participated to the MR"| WaitingForReviewerFeedback([WaitingForReviewerFeedback])
    Reviewable -->|"Thread opened or a Developer voted"| InReview([InReview])
    InReview -->|"All threads resolved and Score > 0"| Acceptable([Acceptable])
    InReview -->|"Inactive for 60 days"| Stale([Stale])
    Acceptable -->|Wait for 72h| Accepted([Accepted])
classDef default fill:#fff,stroke-width:2px
classDef internal stroke-dasharray: 5 5
class Submitted,PipelineReady,QuantumState,PipelineNotFinished internal
style Acceptable color:#8fbc8f,stroke:#8fbc8f
style Accepted color:#009966,stroke:#009966
style InReview color:#FF8800,stroke:#FF8800
style NotCompliant color:#6699cc,stroke:#6699cc
style Reviewable color:#FF610A,stroke:#FF610A
style Stale color:#6699cc,stroke:#6699cc
```

